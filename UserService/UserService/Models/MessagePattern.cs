namespace Models
{
    public class MessagePattern
    {
        private string username;
        private string message;
        private string dateTime;
        public MessagePattern(string username, string message, string dateTime)
        {
            this.username = username;
            this.message = message;
            this.dateTime = dateTime;
        }
        public string Message
        {
            set
            {
                message = value;
            }
            get
            {
                return message;
            }

        }
        public string Username
        {
            set
            {
                username = value;
            }
            get
            {
                return username;
            }
        }
        public string DateTime
        {
            set
            {
                dateTime = value;
            }
            get
            {
                return dateTime;
            }
        }
    }
}